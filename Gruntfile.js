var pkgJson = require("./package.json");
var config = { pkg: pkgJson };
var sass = require('node-sass');

module.exports = function (grunt) {

    require('load-grunt-tasks')(grunt);

    var dnnPath = "../Website/",
        dnnSkinInstallPath = dnnPath + "Install/Skin/",
        dnnSkinPath = dnnPath + "Portals/_default/Skins/",
        containersPath = dnnPath + "Portals/_default/Containers/";

    function copyFilesToSkin(fileArr){
        return  {
            files : [
                {
                    expand: true,
                    src:fileArr,
                    dest : dnnSkinPath + config.pkg.name,
                    filter : "isFile"
                }
            ]
        };
    }

    function copyContainerFiles(fileArr) {
        return {
            files: [
                {
                    cwd: "containers",
                    src: "*",
                    dest: containersPath + config.pkg.name,
                    expand: true,
                    filter: "isFile"
                }
            ]
        };
    }

    // Project configuration.
    grunt.initConfig({
        config : config,
		pkg: config.pkg,
        clean: {
            main: {
                options: {
                    force: true
                },
                src: [dnnSkinPath + config.pkg.name]
            }
        },
        compress: {
            main: {
                options: {
                    archive: dnnSkinInstallPath + "<%= pkg.name %>.<%= pkg.version %>.zip"
                },
                files: [
                    {
                        expand: true,
                        cwd: dnnSkinPath + config.pkg.name,
                        src: [
                            "*.ascx",
                            "*.dnn",
                            "Resources.zip"
                        ]
                    },
                    {
                        expand: true,
                        cwd: 'containers',
                        src: ['*'],
                        dest: '',
                        filter: 'isFile'
                    }
                ]
            },
            resources: {
                options: {
                    archive: dnnSkinPath + config.pkg.name + "/Resources.zip"
                },
                files: [{
                    expand: true,
                    cwd: dnnSkinPath + config.pkg.name,
                    src: [
                        "**",
                        "!*.ascx",
                        "!*.dnn",
                        "!*.manifest",
                        "!**/containers/**",
                        "!**/node_modules/**"
                    ]
                }]
            }
        },
        copy: {
            ascx: copyFilesToSkin("**/*.ascx"),
            cs: copyFilesToSkin("*.cs"),
            css: copyFilesToSkin("css/application.css"),
            doctype: copyFilesToSkin("**/*.doctype.xml"),
            font: copyFilesToSkin("fonts/**/*.{woff,woff2}"),
            js: copyFilesToSkin("js/application.js"),
            img: copyFilesToSkin("img/*.{png,jpg,gif,svg}"),
            navbar: copyFilesToSkin("nav/**/*"),
            containers: copyContainerFiles("containers/*"),
            manifest: {
                options: {
                    processContent: function (content, srcpath) {
                        return grunt.template.process(content);
                    }
                },
                files: [{
                    expand: true,
                    src: "**/*.dnn",
                    dest: dnnSkinPath + config.pkg.name,
                    filter: "isFile"
                }]
            }
        },
        imagemin: {
            dynamic: {
                options: {
                    optimizationLevel: 1,
                    svgoPlugins: [{ removeViewBox: false }]
                },
                files: [{
                    expand: true,
                    cwd: "img/",
                    src: ["**/*.{png,jpg,gif}"],
                    dest: "img/"
                }]
            }
        },
        jshint: {
            files: ["Gruntfile.js", "js/assets/*.js", "!js/assets/vendor/*.js"],
            options: {
                curly: true,
                eqeqeq: true,
                eqnull: true,
                browser: true,
                esversion: 6,
                globals: {
                    jQuery: true,
                    console: true,
                    module: true,
                    document: true
                }
            }
        },
        terser: {
            dist: {
                files: {
                    'js/application.js': 'js/assets/**/*.js'
                }
            }
        },
        sass: {
            options: {
                implementation: sass,
                sourceMap: false,
                outputStyle: "compressed"
            },
            dist: {
                files: {
                    "css/application.css": "css/assets/application.scss"
                }
            }
        },
        sasslint: {
            options: {
                config: ".sass-lint.yml"
            },
            target: [
                "css/assets/**/*.scss",
                "!css/assets/vendor/*.scss"
            ]
        },
        watch: {
            options: {
                livereload: true
            },
            ascx: {
                files: ["**/*.ascx"],
                tasks: ["copy:ascx"]
            },
            cs: {
                files: ["**/*.cs"],
                tasks: ["copy:cs"]
            },
            css: {
                files: ["css/**/*.scss"],
                tasks: ["sasslint", "sass", "copy:css"]
            },
            doctype: {
                files: ["**/*.doctype.xml"],
                tasks: ["copy:doctype"]
            },
            font: {
                files: ["fonts/*.{woff,woff2}"],
                tasks: ["copy:font"]
            },
            img: {
                files: ["img/*.{png,jpg,gif,svg}"],
                tasks: ["imagemin:dynamic", "copy:img"]
            },
            scripts: {
                files: ["js/assets/**/*.js"],
                tasks: ["jshint", "terser", "copy:js"]
            },
            navbar: {
                files: ["nav/**/*"],
                tasks: ["copy:navbar"]
            }
        }
    });

    // Default task(s).
    grunt.registerTask("default", ["build", "watch"]);
	grunt.registerTask("build", ["jshint", "terser", "sasslint", "sass", "imagemin", "copy"]);
	grunt.registerTask("release", ["clean", "build", "compress:resources", "compress:main"]);
};
