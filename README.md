# DNN Skin Template

## Features ##


* [Repo Management](https://bitbucket.org/foremostmedia/dnn-base/wiki/Repo%20Management)
* [SCSS Folder Structure](https://bitbucket.org/foremostmedia/dnn-base/wiki/SCSS%20Standards)
* [Default Variables](https://bitbucket.org/foremostmedia/dnn-base/wiki/Default%20Variables)
* [Style Guide](https://bitbucket.org/foremostmedia/dnn-base/wiki/Style%20Guide)
* [Containers](https://bitbucket.org/foremostmedia/dnn-base/wiki/Containers)

## For understanding of your development setup structure - please [see the wiki](https://bitbucket.org/foremostmedia/dnn-base/wiki/Home)

## Setup

With the migration to NodeJS/Grunt, the following commands must be run
when getting started:

```
$ npm install
```

## Running GruntJS

```
$ grunt
```

Also included is a packager that will create an install package for your skin.  You will need to make the necessary changes to the `package.json` file by updating the skin Name and Version.

```
  "name": "YourSkinName",
  "version": "0.1.0",
```

You will also need to edit the [manifest.dnn](http://www.dnnsoftware.com/wiki/manifests) and update the [Skin Component](http://www.dnnsoftware.com/wiki/manifest-skin-component) file with your `*.ascx` controls.  All other files will be packaged in a `resources.zip` folder for proper installation.

```
    <component type="Skin">
      <skinFiles>
        <skinName><%= pkg.name %></skinName>
        <basePath>Portals\_default\Skins\<%= pkg.name %></basePath>
        <skinFile>
          <name>index.ascx</name>
        </skinFile>
        <skinFile>
          <name>interior.ascx</name>
        </skinFile>
        <skinFile>
          <name>MYOTHERFILE.ascx</name>
        </skinFile>
      </skinFiles>
    </component>
```

To run the packager simply run:

```
$ grunt release
```

## Included Linters

[JSHint](http://jshint.com/docs/) and [SASS-Lint] are included so as to enforce quality of released code. If the linters produce **ANY** errors, the build process will abort.

## Items to exclude from FTP Syncs

All folders/files listed in `.gitignore` except

```
css/application.css
js/application.js
fonts/*
```
## Font Awesome options
* Font Awesome 5 comes with multiple css includes as listed below.
* Dnn base files come with just base Font Awesome enabled but you can change what is included by commenting or deleting unused includes.
```
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/font-awesome/05_15_01/css/fontawesome.min.css" />
<%--<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/font-awesome/05_15_01/css/brands.min.css" />
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/font-awesome/05_15_01/css/duotone.min.css" />
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/font-awesome/05_15_01/css/light.min.css" />
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/font-awesome/05_15_01/css/regular.min.css" />
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/font-awesome/05_15_01/css/solid.min.css" />--%>
```

# Branches #
###CoreLibIntegration###
Dale created a branch called "CoreLibIntegration" which attempts to fix some incompatiblities when running the latest version of node, npm, and npm packages.

As of (10/11/2018):

* The backend team uses the latest version of node (8.11.2) and npm (6.4.1) when they develop mobile apps using "cordova".
* The frontend team uses an older version of node (6.2.0) and npm (5.6.0).
* We are looking into scenarios to solve this inconsistency.  Ultimately, I think it would be best served if the FEDs and BEDs use the same version of node and npm.  This will ensure both teams can compile the repos that use these tools.
* We've decided to leave the status quo alone.  If a BED needs a skin, then a FED will have to build it for them (i.e., clone the repo and build the download package).

**For now, do not delete this branch.  BEDs can use this to get a working copy that they can compile for their purposes (which is very rare but it does happen).**