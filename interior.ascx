<%@ Register TagPrefix="dnn" TagName="BREADCRUMB" Src="~/Admin/Skins/BreadCrumb.ascx" %>

<!-- #include file="partials/header.ascx" -->


<section class="interior">
    <div class="container">
        <div class="site-breadcrumb">
            <dnn:Breadcrumb runat="server" id="dnnBreadcrumb" Separator="&nbsp;|&nbsp;" RootLevel="-1" />
        </div>
        <div id="ContentPane" runat="server"></div>
        <div class="row">
            <div id="LeftContentPane" runat="server" class="col-12 col-lg-8"></div>
            <div id="RightPane" runat="server" class="col-12 col-lg-4"></div>
        </div>
        <div class="row">
            <div id="LeftPane" runat="server" class="col-12 col-lg-4"></div>
            <div id="RightContentPane" runat="server" class="col-12 col-lg-8"></div>
        </div>
        <div class="row">
            <div id="TriColLeftPane" runat="server" class="col-12 col-lg-4"></div>
            <div id="TriColMiddlePane" runat="server" class="col-12 col-lg-4"></div>
            <div id="TriColRightPane" runat="server" class="col-12 col-lg-4"></div>
        </div>
        <div id="BottomPane" runat="server"></div>
    </div>
</section>

<!-- #include file="partials/footer.ascx" -->