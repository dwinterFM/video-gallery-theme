(function($){
    $("body").hide();
    
    $(document).ready(function () {
        
        /* Offcan Menu */
        var htmlHeight = $("html").height(),
            windowHeight = $(window).height();
            menu = $(".horiz-menu");
        
        if(menu.length){
            menu.offCanMenu({
                menuDimensions: {
                    screenWidthTrigger: htmlHeight > windowHeight ? 1200 : 1183
                }
            });
        }
        /* Offcan Menu End */
    });
    
    $(window).load(function() {
        
        /* Page Fade */
        $("body").fadeIn(600);
        var host = window.location.host;
        var menuLinks = $(".horiz-menu a:not([target='_blank'])");
        var linkArray = [];
        var menuArray = [];
        /*ADD ANY PAGE LINKS THAT AREN'T DISPLAYED IN THE MENU HERE*/
        //linkArray.push("/test-page");

        $.each(menuLinks, function(i,v){
            var link = v.href.split(host)[1];
            if(link !== undefined || v.target === "_blank"){
                linkArray.push(v.href.split(host)[1]);
            }
            if(link !== undefined || v.target === "_blank"){
                menuArray.push(v.href);
            }
        });

        $("a").click(function(e) {
            var $that = $(this);
            
            if (linkArray.indexOf($that.attr("href")) > -1 || menuArray.indexOf($that.attr("href")) > -1) {
                e.preventDefault();
                $link = $(this).attr("href");
                $("body").fadeOut(200,function(){
                    window.location =  $link; 
                });
            }
        });
        /* Page Fade End */
    });

    var currentPos = 0;
    $(window).scroll(function () {

        /* Header Hide On Scroll */
        var headerTarget = $("#hideAwayHeader"), 
            headerHeight = headerTarget.outerHeight();
            newPos = $(this).scrollTop();
        
        if (currentPos < newPos && newPos > headerHeight) {
            headerTarget.css("transform", "translateY(-" + headerHeight + "px)");
        } else {
            headerTarget.css("transform", "translateY(0px)");
        }

        currentPos = newPos;
        /* Header Hide On Scroll End */
    });
})(jQuery);