<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/DesktopModules/ForemostMedia/FM_Login_Skin/FMLogin.ascx" %>
<%@ Register TagPrefix="dnn" TagName="COPYRIGHT" Src="~/Admin/Skins/Copyright.ascx" %>

<footer class="site-footer">
    <div class="center container">
        <div class="left-footer">
            <dnn:COPYRIGHT ID="dnnCopyright" runat="server" /> | <a href="/privacy">Privacy</a> | <a href="/terms">Terms</a>
        </div>
        <div class="right-footer">
            Website designed and hosted by <a href="https://www.foremostmedia.com" target="_blank">Foremost Media&reg;</a> | <dnn:LOGIN ID="DnnLogin" CssClass="LoginLink" runat="server" LegacyMode="false"/>
        </div>
    </div>
</footer>