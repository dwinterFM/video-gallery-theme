<%@ Control Language="C#" AutoEventWireup="true" CodeFile="../skin.ascx.cs" Inherits="FmSkin" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" TagName="CONTROLPANEL" Src="~/Admin/Skins/controlpanel.ascx" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.DDRMenu.TemplateEngine" Assembly="DotNetNuke.Web.DDRMenu" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<%@ Register TagPrefix="fm" TagName="FORMSCOOKIE" src="~/DesktopModules/ForemostMedia/fmCookiesTheme/CookiesTheme.ascx" %>
<%@ Register TagPrefix="fm" TagName="CONFIGCREATOR" Src="~/DesktopModules/ForemostMedia/FmConfigurationManager/Creator.ascx" %>

<!-- Access JavascriptLibraries -->
<%@ Register TagPrefix="dnn" TagName="JavaScriptLibraryInclude" Src="~/admin/Skins/JavaScriptLibraryInclude.ascx" %>
<!-- Dependency Includes -->
<dnn:JavaScriptLibraryInclude runat="server" Name="bootstrap" Version="4.1.3" SpecificVersion="LatestMinor" />
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/bootstrap/04_01_03/css/bootstrap.min.css" />
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/font-awesome/05_15_01/css/fontawesome.min.css" />
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/font-awesome/05_15_01/css/regular.min.css" />
<%--<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/font-awesome/05_15_01/css/brands.min.css" />
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/font-awesome/05_15_01/css/duotone.min.css" />
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/font-awesome/05_15_01/css/light.min.css" />
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/font-awesome/05_15_01/css/solid.min.css" />--%>
<dnn:JavaScriptLibraryInclude runat="server" Name="offcanmenu" Version="2.0.8" SpecificVersion="LatestMinor" />
<dnn:DnnCssInclude runat="server" FilePath="~/Resources/Libraries/offcanmenu/02_00_08/css/off-can-menu.css" />
<fm:CONFIGCREATOR runat="server" />

<div id="ControlPanelWrapper">
    <dnn:CONTROLPANEL runat="server" id="cp"  IsDockable="True" />
</div>