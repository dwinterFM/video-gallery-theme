<!-- #include file="header-includes.ascx" -->

<header class="site-header" id="hideAwayHeader">
    <div class="container">
        <div class="Heading row">
            <div class="left-header">
                <div class="logo-wrapper">
                    <a href="https://pubg.com" itemscope="" itemtype="">
                        <img src="<%=SkinPath%>img/PUBG.png" alt="Site Logo" itemprop="logo" />
                        <meta itemprop="name" content="Foremost Media Inc.">
                        <meta itemprop="url" content="https://www.foremostmedia.com">
                        <meta itemprop="telephone" content="999-999-9999">
                        <span itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">
                            <meta itemprop="streetAddress" content="123 Street Lane">
                            <meta itemprop="addressLocality" content="Janesville">
                            <meta itemprop="addressRegion" content="WI">
                            <meta itemprop="postalCode" content="53545">
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>