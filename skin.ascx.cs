using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using DotNetNuke.Web.Client.ClientResourceManagement;
using DotNetNuke.Entities.Users;
using DotNetNuke.UI.Skins;
using DotNetNuke.UI.Skins.Controls;
using DotNetNuke.Framework;

public partial class FmSkin : DotNetNuke.UI.Skins.Skin
{
    protected void Page_Load(object sender, EventArgs e)
    {
        cDefault = Parent.Page as DotNetNuke.Framework.CDefault;

        RegisterJavascript();
        RegisterStylesheets();

        AdditionalClasses();
        SetUpSeo();

    }

    private void RegisterStylesheets()
    {
        ClientResourceManager.RegisterStyleSheet(cDefault, ResolveUrl("css/application.css"), 101);
    }

    private void RegisterJavascript()
    {
        DotNetNuke.Framework.jQuery.RegisterDnnJQueryPlugins(Page);
        ClientResourceManager.RegisterScript(Page, ResolveUrl("js/application.js"), 101);
    }

    #region "These are not the droids you are looking for..."

    private DotNetNuke.Framework.CDefault cDefault;
    private Literal attList;

    private void AdditionalClasses()
    {
        if (cDefault == null) return;

        attList = cDefault.FindControl("attributeList") as Literal;
        var classes = new List<string> { };


        if (IsInternetExplorer())
        {
            classes.Add("ie");
            if (Request.Browser.Version.StartsWith("6"))
                classes.Add("lt-ie9 lt-ie8 lt-ie7");
            else if (Request.Browser.Version.StartsWith("7"))
                classes.Add("lt-ie9 lt-ie8");
            else if (Request.Browser.Version.StartsWith("8"))
                classes.Add("lt-ie9");
        }


        var pageId = PortalSettings.ActiveTab.TabID;
        classes.Add("tab-" + pageId);

        var classValues = string.Join(" ", classes.Distinct());
        if (attList == null)
            cDefault.HtmlAttributes.Add("class", classValues);
        else attList.Text += string.Format(" class='{0}'", classValues);
    }

    private void SetUpSeo()
    {
        var googleAnalyticsFilePath = PortalSettings.HomeDirectoryMapPath + "fmGoogleAnalytics.txt";
        var leadFormixFilePath = PortalSettings.HomeDirectoryMapPath + "fmLeadFormix.txt";
        var scriptText = new List<string>();
        if (File.Exists(googleAnalyticsFilePath))
            scriptText.Add(File.ReadAllText(googleAnalyticsFilePath));
        if (File.Exists(leadFormixFilePath))
            scriptText.Add(File.ReadAllText(leadFormixFilePath));
        Page.Header.Controls.Add(new Literal
        {
            Text = string.Join(Environment.NewLine, scriptText.ToArray())
        });
    }


    private bool IsInternetExplorer()
    {
        return Request.Browser.Browser.Contains("IE") || Request.Browser.Browser.Equals("InternetExplorer", StringComparison.InvariantCultureIgnoreCase);
    }

    public bool isAndroid()
    {
        Match match = Regex.Match(Request.UserAgent.ToLower(), @"^android$",
            RegexOptions.IgnoreCase);

        return match.Success;
    }

    public bool isMobileDevice()
    {
        return (Request.Browser.IsMobileDevice || isAndroid());
    }

    public bool isPhone()
    {
        return (isMobileDevice() && Request.Browser.ScreenPixelsWidth < 768);
    }

    #endregion
}